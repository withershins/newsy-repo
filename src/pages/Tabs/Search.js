import React from 'react';
import { IonPage, IonHeader, IonToolbar, IonTitle } from '@ionic/react';

const Search = () => {
    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonTitle>
                        Search
                    </IonTitle>
                </IonToolbar>
            </IonHeader>
        </IonPage>
    );
};

export default Search;