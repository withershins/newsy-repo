import React from 'react';
import { IonPage, IonHeader, IonToolbar, IonTitle } from '@ionic/react';

const Trending = () => {
    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonTitle>
                        Trending
                    </IonTitle>
                </IonToolbar>
            </IonHeader>
        </IonPage>
    );
};

export default Trending;