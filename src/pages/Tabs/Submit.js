import React from 'react';
import { IonPage, IonHeader, IonToolbar, IonTitle } from '@ionic/react';

const Submit = () => {
    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonTitle>
                        Submit
                    </IonTitle>
                </IonToolbar>
            </IonHeader>
        </IonPage>
    );
};

export default Submit;