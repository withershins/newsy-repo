import React from 'react';
import { IonPage, IonHeader, IonToolbar, IonTitle } from '@ionic/react';

const Profile = () => {
    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonTitle>
                        Profile
                    </IonTitle>
                </IonToolbar>
            </IonHeader>
        </IonPage>
    );
};

export default Profile;